package ch.vonarburg.calculator;

/**
 * Created by Mac on 2017-02-11.
 */

class Operator {
    private static final String INITIAL_VALUE = "";
    static final Operator EQUALS = new Operator("=");
    static final Operator DIVIDE = new Operator("/");
    static final Operator MULTIPLY = new Operator("*");
    static final Operator MINUS = new Operator("-");
    static final Operator PLUS = new Operator("+");
    private String value;

    Operator() {
        this(INITIAL_VALUE);
    }
    private Operator(String s) {
        value = s;
    }

    String init() {
        value = INITIAL_VALUE;
        return value;
    }

    String setValue(String inputValue) {
        value = inputValue;
        return value;
    }

    @Override
    public String toString() {
        if (value != null) {
            return value;
        } else {
            return "";
        }
    }

    @Override
    public boolean equals(Object obj) {
        System.out.println("equals(), value:'" + value + "', obj.toString():'" + obj.toString() + "'");
        return value.equals(obj.toString());
    }

    public Boolean isInitialValue() {
        System.out.println("isInitialValue(), value='" + value + "'");
        return INITIAL_VALUE.equals(value);
    }
}
