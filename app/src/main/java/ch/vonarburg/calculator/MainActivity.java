package ch.vonarburg.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static ch.vonarburg.calculator.ButtonValue.CHARACTER;
import static ch.vonarburg.calculator.ButtonValue.OPERATION;

public class MainActivity extends AppCompatActivity {
    public static final String EYE_CATCHER = "========================";
    public static final String INIT_DISPLAY_ROW = "";

    // attributes visible on activity:
    String[] allButtons = {"buttonPlus", "buttonEquals", "buttonDivide", "buttonMultiply",
            "buttonZero", "buttonOne", "buttonFour", "buttonTwo",
            "buttonThree", "buttonFour", "buttonFive", "buttonSix",
            "buttonSeven", "buttonEight", "buttonDot", "buttonMinus", "buttonNine"};
    private TextView widget_operator;
    private TextView widget_result;
    private TextView widget_newNumber;

    // other class wide used vars:
    Operator lastOperation = new Operator();
    Operator pendingOperation = new Operator();
    Number number = new Number();
    Number operand1 = new Number();
    Number operand2 = new Number();
    String pushedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //System.out.println("START");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        widget_operator = (TextView) findViewById(R.id.operation);
        widget_result = (EditText) findViewById(R.id.result);
        widget_newNumber = (EditText) findViewById(R.id.newNumber);

        //initialize activity fields:
        init_NewNumber();
        init_Result();
        init_Operator();

        //initialize variables:
        init_lastOperation();
        init_operand1();
        init_operand2();
        init_pendingOperation();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(EYE_CATCHER);
                System.out.println("NEW BUTTON PUSH!");
                System.out.println(EYE_CATCHER);

                Button inputButton = (Button) view;
                pushedButton = inputButton.getText().toString();
                System.out.println("inputButton.getText(): '" + pushedButton + "'");
                sysOutAll("N/A");

                if (CHARACTER.equals(pushedButton)) {
                    System.out.println("CHARACTER BUTTON PUSHED!");
                    number.addInputToValue(pushedButton);
                } else if (OPERATION.equals(pushedButton)) {
                    System.out.println("OPERATION BUTTON PUSHED!");
                    if (!number.isInitialValue()){
                        System.out.println("!number.isInitialValue()");
                        performPendingOperation(pushedButton);
                        show_Result();
                    }
                    System.out.println("store new pending operator");
                    storeOperator(pushedButton);
                    System.out.println("display pending operation");
                    show_PendingOperator();
                } else {
                    System.out.println("What button did you push??? '"+pushedButton+"'");
                }
                show_NewNumber();
            }
        };

        //register all buttons:
        for (String allButton : allButtons) {
            int resID = getResources().getIdentifier(allButton, "id", getPackageName());
            Button randomButton = (Button) findViewById(resID);
            randomButton.setOnClickListener(listener);
        }
    }

    void storeOperator(String inputValue) {
        if (!inputValue.equals(Operator.EQUALS.toString())) {
            System.out.println("!Operator.EQUALS.equals(inputValue)");
            lastOperation.setValue(pendingOperation.toString());
        }
        pendingOperation.setValue(inputValue);
    }

    void performPendingOperation(String inputValue) {
        System.out.println("performPendingOperation");
        sysOutAll(inputValue);

        if (operand1.isInitialValue()) {
            System.out.println("operand1.isInitialValue() == true!");
            System.out.println("operand1 = number;");
            operand1.setValue(number.toString());
        } else {
            System.out.println("operand1.isInitialValue() == false!");
            System.out.println("operand2 = number");
            operand2.setValue(number.toString());

            System.out.println("pending operation is equals?");
            if (pendingOperation.equals(Operator.EQUALS)) {
                System.out.println("pendingOperation is EQUALS");
                System.out.println("pendingOperation = operation");
                pendingOperation.setValue(inputValue);

                System.out.println("pending operation is now equals?");
                if (pendingOperation.equals(Operator.EQUALS)) {
                    System.out.println("pendingOperation is EQUALS");
                    System.out.println("operand1 = number");
                    operand1.setValue(number.toString());
                } else {
                    System.out.println("operand1 = performCalc()");
                    operand1.setValue(performCalc());
                }

            } else {
                System.out.println("operand1 = performCalc()");
                operand1.setValue(performCalc());
            }
        }
        System.out.println("operand2 = null");
        operand2.init();
        System.out.println("result = operand1");
        widget_result.setText(operand1.toString());
        System.out.println("clear input field");
        init_NewNumber();
        init_number();
    }

    Double performCalc() {
        System.out.println(EYE_CATCHER);
        System.out.println("performCalc");
        sysOutAll("N/A");

        Double res;
        if (pendingOperation.equals(Operator.DIVIDE)) {
            System.out.println("operand1 / operand2");
            res = operand1.toDouble() / operand2.toDouble();
        } else if (pendingOperation.equals(Operator.MULTIPLY)) {
            System.out.println("operand1 * operand2");
            res = operand1.toDouble() * operand2.toDouble();
        } else if (pendingOperation.equals(Operator.PLUS)) {
            System.out.println("operand1 + operand2");
            res = operand1.toDouble() + operand2.toDouble();
        } else if (pendingOperation.equals(Operator.MINUS)) {
            System.out.println("operand1 - operand2");
            res = operand1.toDouble() - operand2.toDouble();
        } else {
            System.out.println("else!");
            res = 0d;
        }
        System.out.println("result:"+res);
        System.out.println(EYE_CATCHER);
        return res;
    }

    void init_NewNumber() {
        widget_newNumber.setText(INIT_DISPLAY_ROW);
    }
    void init_Operator() {
        widget_operator.setText("");
    }
    void init_Result() {
        widget_result.setText(INIT_DISPLAY_ROW);
    }

    void init_lastOperation() {
        lastOperation.init();
    }
    void init_number() {
        number.init();
    }
    void init_operand1() {
        operand1.init();
    }
    void init_operand2() {
        operand2.init();
    }
    void init_pendingOperation() {
        pendingOperation.init();
    }

    void show_NewNumber() {
        widget_newNumber.setText(number.toString());
    }
    void show_PendingOperator() {
        widget_operator.setText(pendingOperation.toString());
    }
    void show_Result() {
        widget_result.setText(operand1.toString());
    }

    void sysOutAll(String inputValue){
        System.out.println("operand1:           '" + operand1 + "'");
        System.out.println("operand2:           '" + operand2 + "'");
        System.out.println("number:             '" + number + "'");
        System.out.println("pendingOperation:   '" + pendingOperation + "'");
        System.out.println("lastOperation:      '" + lastOperation + "'");
        System.out.println("inputValue:         '" + inputValue + "'");
    }
}


