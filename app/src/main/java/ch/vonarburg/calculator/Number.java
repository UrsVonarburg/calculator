package ch.vonarburg.calculator;

/**
 * Created by Mac on 2017-02-11.
 */

class Number {
    private final String INITIAL_VALUE = "";
    private String value;

    Number(){
        value = INITIAL_VALUE;
    }
    Double addInputToValue(String inputValue){
        if (value.equals(INITIAL_VALUE)){
            value = inputValue;
        } else {
            value += inputValue;
        }
        System.out.println("Number value:"+value);
        return Double.parseDouble(value);
    }
    String init() {
        value = INITIAL_VALUE;
        return value;
    }
    Number setValue(String inputValue) {
        value = inputValue;
        return this;
    }
    Number setValue(Double inputValue) {
        value = inputValue.toString();
        return this;
    }
    @Override
    public String toString() {
        return value;
    }
    Double toDouble() {
        if (INITIAL_VALUE.equals(value)) {
            return null;
        } else {
            Double doubleValue = Double.parseDouble(value);
            System.out.println("doubleValue:" + doubleValue.toString());
            return doubleValue;
        }
    }
    Boolean isInitialValue(){
        System.out.println("isInitialValue(), value='"+value+"'");
        return INITIAL_VALUE.equals(value);
    }
}
