package ch.vonarburg.calculator;

/**
 * Created by Mac on 2017-02-10.
 */

enum ButtonValue {
    OPERATION("+", "-", "*", "/", "="),
    CHARACTER("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".");

    private String[] possibleValues;

    ButtonValue(String... s) {
        possibleValues = s;
    }

    public String toString() {
        return possibleValues[0];
    }

    public Boolean equals(String s) {
        for (String value : possibleValues) {
            if (value.equals(s)) {
                return true;
            }
        }
        return false;
    }
}

//public final class ButtonValue{
//    public static final String EYE_CATCHER          = "========================";
//    public static final String INIT_DISPLAY_ROW     = "";
//    public static final String OPERATION_EQUAL      = "=";
//    public static final String OPERATION_DIVIDE     = "/";
//    public static final String OPERATION_MULTIPLY   = "*";
//    public static final String OPERATION_MINUS      = "-";
//    public static final String OPERATION_PLUS       = "+";
//    public static final String CHAR_ZERO            = "0";
//    public static final String CHAR_ONE             = "1";
//    public static final String CHAR_TWO             = "2";
//    public static final String CHAR_THREE           = "3";
//    public static final String CHAR_FOUR            = "4";
//    public static final String CHAR_FIVE            = "5";
//    public static final String CHAR_SIX             = "6";
//    public static final String CHAR_SEVEN           = "7";
//    public static final String CHAR_EIGHT           = "8";
//    public static final String CHAR_NINE            = "9";
//    public static final String CHAR_DOT             = ".";
//    //public static final String CHAR_COMMA           = ",";
//}
